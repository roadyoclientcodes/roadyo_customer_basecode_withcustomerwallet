//
//  NSBundle+ForceLocalization.h
//  UBER
//
//  Created by Rahul Sharma on 09/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (ForceLocalization)

+(void)setLanguage:(NSString*)language;


@end
