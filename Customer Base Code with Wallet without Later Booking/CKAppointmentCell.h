//
//  CKAppointmentCell.h
//  privMD
//
//  Created by Rahul Sharma on 27/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundedImageView.h"
@interface CKAppointmentCell : UITableViewCell

@property(strong,nonatomic) UIImageView *docApptImage;
@property(strong,nonatomic) UIView *containerView;
@property(strong,nonatomic) UIView *topView;
@property(strong, nonatomic) UIView *headerView;
@property(strong,nonatomic) UILabel *docApptAddr;
@property(strong,nonatomic) UILabel *docApptName;
@property(strong,nonatomic) UILabel *appDateTime;
@property(strong,nonatomic) UILabel *docApptDropAddr;
@property(strong,nonatomic) UILabel *distance;
@property(strong,nonatomic) UILabel *status;
@property(strong,nonatomic) UILabel *dateLabel;

@property(strong,nonatomic) UILabel *totalAmount;
@property(strong,nonatomic) UIImageView *imgPickLocation;
@property(strong,nonatomic) UIImageView *imgDropLocation;
@property(strong,nonatomic) UIImageView *imgDistance;
@property(strong,nonatomic) UIImageView *imgTime;

@property(strong, nonatomic)  UIActivityIndicatorView *activityIndicator;


@end
