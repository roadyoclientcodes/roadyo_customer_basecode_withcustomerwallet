//
//  surgePricePopUp.h
//  RoadyoDispatch
//
//  Created by 3Embed on 09/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface surgePricePopUp : UIView

- (IBAction)cancelBtnAction:(id)sender;
- (IBAction)acceptFareBtnAction:(id)sender;
@property(strong, nonatomic)IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *surgeRoundView;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImage;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *vehicleNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *surgePrice;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;


@property (nonatomic, copy)   void (^onCompletion)(NSInteger acceptOrRejects);
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;

@end
