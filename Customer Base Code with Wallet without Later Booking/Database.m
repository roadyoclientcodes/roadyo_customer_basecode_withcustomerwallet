//
//  Database.m
//  privMD
//
//  Created by Rahul Sharma on 20/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "Database.h"
#import "Entity.h"
#import "PatientAppDelegate.h"
#import "SourceAddress.h"
#import "DestinationAddress.h"

@implementation Database

-(void)makeDataBaseEntry:(NSDictionary *)dictionary
{
	NSError *error;
	PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	Entity *entity = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:context];
    
    [entity setExpMonth:flStrForObj(dictionary [@"exp_month"])];
    [entity setExpYear:flStrForObj(dictionary [@"exp_year"])];
    [entity setIdCard:flStrForObj(dictionary [@"id"])];
    [entity setCardtype:flStrForObj(dictionary [@"type"])];
    [entity setLast4:flStrForObj(dictionary[@"last4"])];
    [entity setIsDefault:flStrForObj(dictionary[@"isDefault"])];
 
	BOOL isSaved = [context save:&error];
	if (isSaved)
    {
    }
	else
    {
	}
    
}
+ (NSArray *)getCardDetails;
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Entity" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    return result;
    
    return nil;
}

+ (BOOL)DeleteCard:(NSString*)Campaign_id
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"Entity" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", Campaign_id];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
    if ([context save:&error]) {
        return YES;
    }
    else {
        return NO;
    }
    return NO;
}

+(BOOL)updateCardDetailsForCardId:(NSString *)cardID andStatus:(NSString *)isDefault {
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"Entity" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *error;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", cardID];
    [fetch setPredicate:pred];
    Entity *card = [[context executeFetchRequest:fetch error:nil] lastObject];
    [card setIsDefault:isDefault];
    BOOL isSaved = [context save:&error];
    return isSaved;
}

/**
 *  On logout it will delete all cards
 */

+ (void)DeleteAllCard
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"Entity" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    //  NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", Campaign_id];
    //[fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}

/**
 *  Adding Address to the Database and managing that
 */

-(void)addSourceAddressInDataBase:(NSDictionary *)dictionary
{
    NSArray *addressArr = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
    if(addressArr.count)
    {
        for (int arrCount = 0; arrCount < addressArr.count;  arrCount++)
        {
            id address =  addressArr[arrCount];
            SourceAddress *add = (SourceAddress*)address;
            if([add.keyId isEqualToString:dictionary[(@"place_id")]] )
            {
                return;
            }
        }
    }
    NSDictionary *location = dictionary[@"geometry"][@"location"];
    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    SourceAddress *entity = [NSEntityDescription insertNewObjectForEntityForName:@"SourceAddress" inManagedObjectContext:context];
    
    NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr(dictionary[@"name"])];
    NSString *add2 = @"";
    NSString *zipCode =@"";
    NSArray *arr = [dictionary objectForKey:@"address_components"];
    for (int addrCount=1 ; addrCount< [arr count]; addrCount++)
    {
        NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
        if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
        {
            add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
        }
        if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
        {
            zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
        }
    }
    if ([add2 hasPrefix:@", "]) {
        add2 = [add2 substringFromIndex:2];
    }
    
    if ([add2 hasSuffix:@", "]) {
        add2 = [add2 substringToIndex:[add2 length]];
    }
    if (add1.length == 0)
    {
        NSArray *arr = dictionary[@"address_components"];
        NSString *add2 =@"";
        for (int addrCount=0 ; addrCount< [arr count]; addrCount++)
        {
            NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
            if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
            {
                add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
            }
        }
        
        if ([add2 hasPrefix:@","]) {
            add2 = [add2 substringFromIndex:1];
        }
        
        if ([add2 hasSuffix:@", "]) {
            add2 = [add2 substringToIndex:[add2 length]-2];
        }
        if(add2.length)
        {
            add1 = [NSString stringWithFormat:@"%@, %@",add1, add2];
            add2 = @"";
        }
        else
        {
            add1 = [NSString stringWithFormat:@"%@ %@",add1, add2];
            add2 = @"";
        }
    }
//        [entity setSrcAddress:flStrForStr(dictionary[@"name"])];
//        [entity setSrcAddress2:flStrForStr(dictionary[@"formatted_address"])];

    [entity setSrcAddress:flStrForStr(add1)];
    [entity setSrcAddress2:flStrForStr(add2)];
    [entity setSrcLatitude:[NSNumber numberWithDouble:[[location objectForKey:@"lat"] doubleValue]]];
    [entity setSrcLongitude:[NSNumber numberWithDouble:[[location objectForKey:@"lng"] doubleValue]]];
    [entity setKeyId:flStrForStr(dictionary[(@"place_id")])];
    [entity setZipCode:flStrForStr(zipCode)];
    BOOL isSaved = [context save:&error];
    if (isSaved) {
    }
}

+(NSArray *)getSourceAddressFromDataBase
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SourceAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
	return result;
    
}

+ (void)deleteAllSourceAddress
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"SourceAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}

-(void)addDestinationAddressInDataBase:(NSDictionary *)dictionary
{
    NSArray *addressArr = [[NSMutableArray alloc] initWithArray:[Database getDestinationAddressFromDataBase]];
    if(addressArr.count)
    {
        for (int arrCount = 0; arrCount < addressArr.count;  arrCount++)
        {
            id address =  addressArr[arrCount];
            SourceAddress *add = (SourceAddress*)address;
            if([add.keyId isEqualToString:dictionary[(@"place_id")]] )
            {
                return;
            }
        }
    }
    NSDictionary *location = dictionary[@"geometry"][@"location"];
    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    DestinationAddress *entity = [NSEntityDescription insertNewObjectForEntityForName:@"DestinationAddress" inManagedObjectContext:context];
    
    NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr(dictionary[@"name"])];
    NSString *add2 = @"";
    NSString *zipCode =@"";
    NSArray *arr = [dictionary objectForKey:@"address_components"];
    for (int addrCount=1 ; addrCount< [arr count]; addrCount++)
    {
        NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
        if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
        {
            add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
        }
        if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
        {
            zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
        }
    }
    if ([add2 hasPrefix:@", "]) {
        add2 = [add2 substringFromIndex:2];
    }
    
    if ([add2 hasSuffix:@", "]) {
        add2 = [add2 substringToIndex:[add2 length]];
    }
    if (add1.length == 0)
    {
        NSArray *arr = dictionary[@"address_components"];
        NSString *add2 =@"";
        for (int addrCount=0 ; addrCount< [arr count]; addrCount++)
        {
            NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
            if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
            {
                add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
            }
        }
        
        if ([add2 hasPrefix:@","]) {
            add2 = [add2 substringFromIndex:1];
        }
        
        if ([add2 hasSuffix:@", "]) {
            add2 = [add2 substringToIndex:[add2 length]-2];
        }
        if(add2.length)
        {
            add1 = [NSString stringWithFormat:@"%@, %@",add1, add2];
            add2 = @"";
        }
        else
        {
            add1 = [NSString stringWithFormat:@"%@ %@",add1, add2];
            add2 = @"";
        }
    }
    
//    [entity setDesAddress:flStrForStr(dictionary[@"name"])];
//    [entity setDesAddress2:flStrForStr(dictionary[@"formatted_address"])];
    
    [entity setDesAddress:flStrForStr(add1)];
    [entity setDesAddress2:flStrForStr(add2)];
    [entity setDesLatitude:[NSNumber numberWithDouble:[[location objectForKey:@"lat"] doubleValue]]];
    [entity setDesLongitude:[NSNumber numberWithDouble:[[location objectForKey:@"lng"] doubleValue]]];
    [entity setKeyId:flStrForStr(dictionary[@"place_id"])];
    [entity setZipCode:flStrForStr(dictionary[@"zipCode"])];
    BOOL isSaved = [context save:&error];
    if (isSaved)
    {
    }
}

+(NSArray *)getDestinationAddressFromDataBase
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DestinationAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
	return result;
}

+ (void)deleteAllDestinationAddress
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"DestinationAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}

+ (BOOL)DeleteDestinationAddress:(NSString*)addressTag
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"DestinationAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", addressTag];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
    if ([context save:&error]) {
        return YES;
    }
    else {
        return NO;
    }
    return NO;
}


@end
