//
//  CKAppointmentCell.m
//  privMD
//
//  Created by Rahul Sharma on 27/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CKAppointmentCell.h"
#define imageOffset 2.5
@implementation CKAppointmentCell
@synthesize docApptImage;
@synthesize docApptName;
@synthesize docApptAddr;
@synthesize docApptDropAddr;
@synthesize appDateTime;
@synthesize distance;
@synthesize totalAmount;
@synthesize imgDistance;
@synthesize imgDropLocation;
@synthesize imgPickLocation;
@synthesize imgTime;
@synthesize activityIndicator;
@synthesize status;
@synthesize containerView,topView,dateLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {// Initialization code
        if (!containerView) {
            
            containerView = [[UIView alloc]initWithFrame:CGRectMake(10,0,300,170)];
            [containerView setBackgroundColor:UIColorFromRGB(0xffffff)];
            containerView.layer.borderColor = [[UIColor blackColor] colorWithAlphaComponent:0.5].CGColor;
            containerView.layer.borderWidth = 0.5f;
            [self.contentView addSubview:containerView];
        }
        if (!topView) {
            
            topView = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,40)];
            topView.backgroundColor = UIColorFromRGB(0xe5e5e5);
            [containerView addSubview:topView];
            
        }
        if (!dateLabel) {
            
            dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,0,130,40)];
            [Helper setToLabel:dateLabel Text:@"" WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x656565)];
            [topView addSubview:dateLabel];
        }
        if(!status)
        {
            status = [[UILabel alloc]initWithFrame:CGRectMake(140,0,158,40)];
            [Helper setToLabel:status Text:@"" WithFont:Roboto_Bold FSize:10 Color:UIColorFromRGB(0x656565)];
            status.textAlignment = NSTextAlignmentRight;
            [topView addSubview:status];
        }
        if(!docApptImage)
        {
            docApptImage = [[UIImageView alloc]initWithFrame:CGRectMake(5,topView.frame.size.height+topView.frame.origin.y+15,70,70)];
            [containerView addSubview:docApptImage];
        }
        if(!totalAmount) //total amount field
        {
            totalAmount = [[UILabel alloc]initWithFrame:CGRectMake(90,docApptImage.frame.size.height+docApptImage.frame.origin.y+20,80,30)];
            [Helper setToLabel:totalAmount Text:@"" WithFont:Roboto_Bold FSize:14 Color:UIColorFromRGB(0x000000)];
            [containerView addSubview:totalAmount];
        }
        
        if(!docApptName) //driver name
        {
            docApptName = [[UILabel alloc]initWithFrame:CGRectMake(90,topView.frame.size.height+topView.frame.origin.y+4, 200, 20)];
            [Helper setToLabel:docApptName Text:@"" WithFont:Roboto_Regular FSize:15 Color:UIColorFromRGB(0x000000)];
            [containerView addSubview:docApptName];
        }
        if(!imgPickLocation) //pick
        {
            UIImage *img = [UIImage imageNamed:@"calender_pickuplocation_icon"];
            imgPickLocation = [[UIImageView alloc]initWithFrame:CGRectMake(90,docApptName.frame.origin.y+docApptName.frame.size.height+imageOffset +5,15, 15)];
            imgPickLocation.image = img;
            [containerView addSubview:imgPickLocation];
        }
        if(!docApptAddr)//pick
        {
            docApptAddr = [[UILabel alloc]initWithFrame:CGRectMake(115,docApptName.frame.origin.y+docApptName.frame.size.height,190, 40)];
            [Helper setToLabel:docApptAddr Text:@"" WithFont:Roboto_Regular FSize:12 Color:UIColorFromRGB(0x000000)];
            docApptAddr.numberOfLines = 2;
            [containerView addSubview:docApptAddr];
        }
        if(!imgDropLocation)//drop
        {
            UIImage *img = [UIImage imageNamed:@"calender_dropoff_icon"];
            imgDropLocation = [[UIImageView alloc]initWithFrame:CGRectMake(90,docApptAddr.frame.origin.y+docApptAddr.frame.size.height+imageOffset+5,15, 15)];
            imgDropLocation.image = img;
            [containerView addSubview:imgDropLocation];
        }
        if(!docApptDropAddr)//drop
        {
            docApptDropAddr = [[UILabel alloc]initWithFrame:CGRectMake(115,docApptAddr.frame.origin.y+docApptAddr.frame.size.height,190,40)];
            [Helper setToLabel:docApptDropAddr Text:@"" WithFont:Roboto_Regular FSize:12 Color:UIColorFromRGB(0x000000)];
            docApptDropAddr.numberOfLines = 2;
            [containerView addSubview:docApptDropAddr];
        }
        
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(60/2-20/2, 60/2-20/2, 20, 20)];
        [self.docApptImage addSubview:activityIndicator];
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
