//
//  WalletViewController.h
//  RoadyoDispatch
//
//  Created by 3Embed on 20/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UILabel *walletLabel1;
@property (weak, nonatomic) IBOutlet UILabel *walletLabel2;


@property (weak, nonatomic) IBOutlet UIView *balanceView;
@property (weak, nonatomic) IBOutlet UILabel *balanceStatusLabel;

@property (weak, nonatomic) IBOutlet UIView *balanceAddView;
@property (weak, nonatomic) IBOutlet UILabel *addMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentBalanceLabel;

@property (weak, nonatomic) IBOutlet UITextField *addMoneyTextField;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn1;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn2;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn3;

@property (weak, nonatomic) IBOutlet UIButton *promoCodeCheckMarkBtn;
- (IBAction)promoCodeCheckMarkBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *havePromoLabel;

@property (weak, nonatomic) IBOutlet UIView *promoCodeView;
@property (weak, nonatomic) IBOutlet UITextField *promoCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *promoCodeApplyBtn;


//- (IBAction)currentBalanceBtnAction:(id)sender;
- (IBAction)addMoneyBtn1Action:(id)sender;
- (IBAction)addMoneyBtn2Action:(id)sender;
- (IBAction)addMoneyBtn3Action:(id)sender;
- (IBAction)promoCodeApplyBtnAction:(id)sender;

- (IBAction)addMoneyBtnAction:(id)sender;

@end
