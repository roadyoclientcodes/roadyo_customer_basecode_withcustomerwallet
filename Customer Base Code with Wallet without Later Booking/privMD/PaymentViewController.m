//
//  PaymentViewController.m
//  privMD
//
//  Created by Rahul Sharma on 02/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PaymentViewController.h"
#import "CardLoginViewController.h"
#import "XDKAirMenuController.h"
#import "PaymentDetailsViewController.h"
#import "PaymentCell.h"
#import "Database.h"
#import "Entity.h"
#import "PatientViewController.h"
#import "PaymentTableViewCell.h"
#import "PMDReachabilityWrapper.h"
#import "CustomNavigationBar.h"
#import "PubNubWrapper.h"
#import "PaymentViewController1.h"


@interface PaymentViewController () <CustomNavigationBarDelegate, UIAlertViewDelegate> {
    float keyboardHeight;
    NSString *currencyStr;
    UITapGestureRecognizer *tap;
    PaymentTableViewCell *cell;
    NSIndexPath *defaultPaymentMethodIndex;
    NSString *amountToAdd;
}

@property(strong,nonatomic) UIButton *addPaymentbutton;
@property (strong,nonatomic)  UITextField *activeTextField;

@end

@implementation PaymentViewController
@synthesize addPaymentbutton;
@synthesize paymentTableView;
@synthesize callback;
@synthesize addMoneytextField, addMoneyBtn1, addMoneyBtn2, addMoneyBtn3;
@synthesize walletView, walletHeadingView, walletHeadingLabel;
@synthesize currentBalanceStatusLabel, currentBalanceAmountLabel;
@synthesize activeTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}
#pragma mark - ViewLifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    cell = nil;
    amountToAdd = @"0";
    self.view.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.00];
    [self addCustomNavigationBar];
    [self getWalletCurrentMoney];
    currencyStr =  [PatientGetLocalCurrency getCurrencyLocal:[@"0" floatValue]];
    NSRange range = [currencyStr rangeOfString:@" 0.00"];
    if (range.location != NSNotFound) {
        currencyStr = [currencyStr substringToIndex:range.location];
    }
    
    [Helper setButton:addMoneyBtn1 Text:[NSString stringWithFormat:@"%@ 100", currencyStr] WithFont:Roboto_Bold FSize:12 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [Helper setButton:addMoneyBtn2 Text:[NSString stringWithFormat:@"%@ 200", currencyStr] WithFont:Roboto_Bold FSize:12 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [Helper setButton:addMoneyBtn3 Text:[NSString stringWithFormat:@"%@ 300", currencyStr] WithFont:Roboto_Bold FSize:12 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    
    addMoneytextField.placeholder = [NSString stringWithFormat:@"%@ 100", currencyStr];
    
//    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
//    if ( [reachability isNetworkAvailable]) {
//        [self sendServicegetCardDetail];
//    } else {
//        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
//    }
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
    context = [appDelegate managedObjectContext];
    if (context!=nil) {
        arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
    }
    [paymentTableView reloadData];
    if (defaultPaymentMethodIndex != nil) {
        [paymentTableView selectRowAtIndexPath:defaultPaymentMethodIndex animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self tableView:paymentTableView didSelectRowAtIndexPath:defaultPaymentMethodIndex];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    CGRect frame = self.paymentTableView.frame;
    frame.size.height = self.paymentTableView.contentSize.height;
    frame.origin.y = 0;
    self.paymentTableView.frame = frame;
    frame = self.walletView.frame;
    frame.origin.y = CGRectGetMaxY(self.paymentTableView.frame);
    self.walletView.frame = frame;
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.contentSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, CGRectGetMaxY(walletView.frame));

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)cardDetailsButtonClicked:(id)sender {
    UIButton *mBtn = (UIButton *)sender;
    isGoingDelete = YES;
    _dict = _arrayContainingCardInfo[mBtn.tag];
    [self performSegueWithIdentifier:@"goToPaymentDetail" sender:self];
}

-(void)cardDetailsClicked:(NSDictionary *)getDict {
    _dict =  [getDict mutableCopy];
    [self performSegueWithIdentifier:@"goToPaymentDetail" sender:self];
}

-(void)addPayment {
    [self performSegueWithIdentifier:@"goTocardScanController" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"goToPaymentDetail"]) {
        PaymentDetailsViewController *PDVC = [segue destinationViewController];
        PDVC.containingDetailsOfCard = _dict;
        PDVC.callback = ^() {
            [self sendServicegetdeleteCard];
        };
    } else if ([segue.identifier isEqualToString:@"walletHistoryVC"]) {
        NSLog(@"Add data to next VC");
    } else {
        CardLoginViewController *CLVC = [segue destinationViewController];
        CLVC.isComingFromPayment = 1;
    }
}

- (void)menuButtonPressedAccount {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

-(void)cancelButtonPressedAccount {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) addCustomNavigationBar {
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"PAYMENT", @"PAYMENT")];
    if(_isComingFromSummary == YES) {
        [customNavigationBarView setLeftBarButtonTitle:@""];
        [customNavigationBarView hideLeftMenuButton:YES];
        [customNavigationBarView setleftBarButtonImage:[UIImage imageNamed:@"payment_cancel_btn_on"] :[UIImage imageNamed:@"payment_cancel_btn_off"]];
    }
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    if(_isComingFromSummary == YES)
        [self cancelButtonPressedAccount];
    else
        [self menuButtonPressedAccount];
}


#pragma mark - WebService call

-(void)sendServicegetCardDetail {
    NSString *deviceId;
    if (IS_SIMULATOR)
        deviceId = kPMDTestDeviceidKey;
    else
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    NSString *sesstionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    else
        pToken = @"";
    
    NSDictionary *params = @{@"ent_sess_token": sesstionToken,
                             @"ent_dev_id": deviceId,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getCards"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self getCardDetails:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

-(void)getCardDetails:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        return;
    } else {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([dictResponse[@"errFlag"] intValue] == 1 && ([dictResponse[@"errNum"] intValue] == 6 || [dictResponse[@"errNum"] intValue] == 7)) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [XDKAirMenuController relese];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
        }
        else  if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            _arrayContainingCardInfo = dictResponse[@"cards"];
            [Database DeleteAllCard];
            [arrDBResult removeAllObjects];
            [self addInDataBase];
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
            NSString *currentBal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dictResponse[@"walletBal"]) floatValue]];
            [addMoneytextField resignFirstResponder];
            currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
            if ([dictResponse[@"walletBal"] integerValue] <= 0) {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0xeb1313)];
            } else {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
            }
            [paymentTableView reloadData];
            if (defaultPaymentMethodIndex != nil) {
                [paymentTableView selectRowAtIndexPath:defaultPaymentMethodIndex animated:YES scrollPosition:UITableViewScrollPositionNone];
                [self tableView:paymentTableView didSelectRowAtIndexPath:defaultPaymentMethodIndex];
            }
            CGRect frame = self.paymentTableView.frame;
            frame.size.height = self.paymentTableView.contentSize.height;
            frame.origin.y = 0;
            self.paymentTableView.frame = frame;
            frame = self.walletView.frame;
            frame.origin.y = CGRectGetMaxY(self.paymentTableView.frame);
            self.walletView.frame = frame;
            self.mainScrollView.contentSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, CGRectGetMaxY(walletView.frame));
        } else if([response[@"errFlag"] integerValue] == 1 && [response[@"errNum"] integerValue] == 51) {
            [paymentTableView reloadData];
            self.mainScrollView.contentSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, CGRectGetMaxY(walletView.frame));
            if (defaultPaymentMethodIndex != nil) {
                [paymentTableView selectRowAtIndexPath:defaultPaymentMethodIndex animated:YES scrollPosition:UITableViewScrollPositionNone];
                [self tableView:paymentTableView didSelectRowAtIndexPath:defaultPaymentMethodIndex];
            }
        }
    }
}

-(void)sendServicegetdeleteCard {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [[ProgressIndicator sharedInstance] showPIOnView:window withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    NSString *deviceId;
    if (IS_SIMULATOR)
        deviceId = kPMDTestDeviceidKey;
    else
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    NSString *parameters = [NSString stringWithFormat:@"ent_sess_token=%@&ent_dev_id=%@&ent_cc_id=%@&ent_date_time=%@",[[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken],deviceId,_dict[@"id"],[Helper getCurrentDateTime]];
    NSString *removeSpaceFromParameter=[Helper removeWhiteSpaceFromURL:parameters];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@removeCard",BASE_URL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:[[removeSpaceFromParameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]
                             dataUsingEncoding:NSUTF8StringEncoding
                             allowLossyConversion:YES]];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(getdeleteCardResponse:)];
}

-(void)getdeleteCardResponse:(NSDictionary *)response {
    if (!response) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    } else {
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
//            [_arrayContainingCardInfo removeAllObjects];
//            [self sendServicegetCardDetail];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}


-(void) getWalletCurrentMoney {
    NSString *sid = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:KNSUSid]);
    NSDictionary *params = @{
                             @"ent_sid":flStrForStr(sid),
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"GetWalletMoney"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getWalletCurrentMoneyResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Checking Current Balance...", @"Checking Current Balance...")];
}

-(void) getWalletCurrentMoneyResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        NSString *currentBal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
        if ([responseDict[@"amount"] integerValue] <= 0) {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0xeb1313)];
        } else {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
        }
    }
}

-(void) addMoneyToWallet:(NSString *)amount withCardID:(NSString *)cardID {
    NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
    NSString *sessionToken = [udPlotting objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
        deviceID = kPMDTestDeviceidKey;
    else
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];

    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_amount":flStrForStr(amount),
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_token": flStrForStr(cardID)
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"AddMoneyToWallet"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self addMoneyToWalletResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Adding Money...", @"Adding Money...")];
}

-(void) addMoneyToWalletResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil){
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [addMoneyBtn1 setSelected: NO];
        [addMoneyBtn2 setSelected: NO];
        [addMoneyBtn3 setSelected: NO];
        
        NSString *currentBal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
        addMoneytextField.text = @"";
        currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
        if ([responseDict[@"amount"] integerValue] <= 0) {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0xeb1313)];
        } else {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
        }
        [self openHistoryAction:self];
    }
}



#pragma mark -UITABLEVIEW DELEGATE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (kPMDPaymentType) {
        return arrDBResult.count+1;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"paymentTableViewCell";
    cell = (PaymentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell==nil) {
        cell =[[PaymentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    tableView.backgroundColor=[UIColor clearColor];
    
    if(indexPath.row == arrDBResult.count) {
        [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Add Credit Card", @"Add Credit Card") WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x666666)];
        cell.iconImageView.image = [UIImage imageNamed:@"payment_add"];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"walletHistory_off"]];
        
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
            cell.textLabel.textAlignment = NSTextAlignmentRight;
        else
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
    } else {
        Entity *card = arrDBResult[indexPath.row];
        NSString *findRangeForString = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Ends with", @"Ends with"), card.last4];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:findRangeForString attributes:nil];
        NSRange range1 = [findRangeForString rangeOfString:NSLocalizedString(@"Ends with", @"Ends with")];
        NSRange range2 = [findRangeForString rangeOfString:card.last4];
        [attributedString addAttribute: NSForegroundColorAttributeName
                                 value: UIColorFromRGB(0x666666)
                                 range: range1];
        [attributedString addAttribute: NSFontAttributeName
                                 value:  [UIFont fontWithName:Lato size:14]
                                 range: range1];
        
        
        [attributedString addAttribute: NSForegroundColorAttributeName
                                 value: UIColorFromRGB(0x333333)
                                 range: range2];
        
        [attributedString addAttribute: NSFontAttributeName
                                 value:  [UIFont fontWithName:Lato size:14]
                                 range: range2];
        
        cell.itemLabel.attributedText = attributedString;
        cell.iconImageView.image = [self setPlaceholderToCardType:card.cardtype];
        if ([card.isDefault integerValue] == 1) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"new_correct_icon"]];
            defaultPaymentMethodIndex = indexPath;
        } else {
            cell.accessoryView = nil;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    return cell;
}

//    if (kPMDPaymentType) {
//        if(indexPath.row == arrDBResult.count+1) {
//            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Add Credit Card", @"Add Credit Card") WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x666666)];
//            cell.iconImageView.image = [UIImage imageNamed:@"payment_add"];
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//            cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"walletHistory_off"]];
//
//            if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
//                cell.textLabel.textAlignment = NSTextAlignmentRight;
//            else
//                cell.textLabel.textAlignment = NSTextAlignmentLeft;
//        } else if (indexPath.row == arrDBResult.count) {
//            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Cash", @"Cash") WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x666666)];
//            cell.iconImageView.image = [UIImage imageNamed:@"new_cash_icon"];
//            cell.accessoryView = nil;
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        } else {
//            Entity *card = arrDBResult[indexPath.row];
//            NSString *findRangeForString = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Ends with", @"Ends with"), card.last4];
//            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:findRangeForString attributes:nil];
//            NSRange range1 = [findRangeForString rangeOfString:NSLocalizedString(@"Ends with", @"Ends with")];
//            NSRange range2 = [findRangeForString rangeOfString:card.last4];
//            [attributedString addAttribute: NSForegroundColorAttributeName
//                                     value: UIColorFromRGB(0x666666)
//                                     range: range1];
//            [attributedString addAttribute: NSFontAttributeName
//                                     value:  [UIFont fontWithName:Lato size:14]
//                                     range: range1];
//
//
//            [attributedString addAttribute: NSForegroundColorAttributeName
//                                     value: UIColorFromRGB(0x333333)
//                                     range: range2];
//
//            [attributedString addAttribute: NSFontAttributeName
//                                     value:  [UIFont fontWithName:Lato size:14]
//                                     range: range2];
//
//            cell.itemLabel.attributedText = attributedString;
//            cell.iconImageView.image = [self setPlaceholderToCardType:card.cardtype];
//            if ([card.isDefault integerValue] == 1) {
//                cell.accessoryType = UITableViewCellAccessoryCheckmark;
//                cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"new_correct_icon"]];
//                defaultPaymentMethodIndex = indexPath;
//            } else {
//                cell.accessoryView = nil;
//                cell.accessoryType = UITableViewCellAccessoryNone;
//            }
//        }
//    } else if(!kPMDPaymentType && kPMDCardOrCash) {
//        if(indexPath.row == arrDBResult.count) {
//            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Add Credit Card", @"Add Credit Card") WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x666666)];
//            cell.iconImageView.image = [UIImage imageNamed:@"payment_add"];
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//            cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"walletHistory_off"]];
//
//            if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
//                cell.textLabel.textAlignment = NSTextAlignmentRight;
//            else
//                cell.textLabel.textAlignment = NSTextAlignmentLeft;
//        } else {
//            Entity *card = arrDBResult[indexPath.row];
//            NSString *findRangeForString = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Ends with", @"Ends with"), card.last4];
//            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:findRangeForString attributes:nil];
//            NSRange range1 = [findRangeForString rangeOfString:NSLocalizedString(@"Ends with", @"Ends with")];
//            NSRange range2 = [findRangeForString rangeOfString:card.last4];
//            [attributedString addAttribute: NSForegroundColorAttributeName
//                                     value: UIColorFromRGB(0x666666)
//                                     range: range1];
//            [attributedString addAttribute: NSFontAttributeName
//                                     value:  [UIFont fontWithName:Lato size:14]
//                                     range: range1];
//
//
//            [attributedString addAttribute: NSForegroundColorAttributeName
//                                     value: UIColorFromRGB(0x333333)
//                                     range: range2];
//
//            [attributedString addAttribute: NSFontAttributeName
//                                     value:  [UIFont fontWithName:Lato size:14]
//                                     range: range2];
//
//            cell.itemLabel.attributedText = attributedString;
//            cell.iconImageView.image = [self setPlaceholderToCardType:card.cardtype];
//            if ([card.isDefault integerValue] == 1) {
//                cell.accessoryType = UITableViewCellAccessoryCheckmark;
//                cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"new_correct_icon"]];
//                defaultPaymentMethodIndex = indexPath;
//            } else {
//                cell.accessoryView = nil;
//                cell.accessoryType = UITableViewCellAccessoryNone;
//            }
//        }
//    }
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    headerView.backgroundColor = UIColorFromRGB(0x000000);
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, [UIScreen mainScreen].bounds.size.width-20, 30)];
    [Helper setToLabel:headerLabel Text:NSLocalizedString(@"SAVED CARDS", @"SAVED CARDS") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0xFFFFFF)];
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
        headerLabel.textAlignment = NSTextAlignmentRight;
    else
        headerLabel.textAlignment = NSTextAlignmentLeft;
    
    [headerView addSubview:headerLabel];
    return headerView;
}

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == arrDBResult.count) {
        [self addPayment];
    } else {
        cell = [tableView1 cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"new_correct_icon"]];
        Entity *card = arrDBResult[defaultPaymentMethodIndex.row];
        NSString *cardID = card.idCard;
        [Database updateCardDetailsForCardId:cardID andStatus:@"0"];
        
        defaultPaymentMethodIndex = indexPath;
        
        card = arrDBResult[defaultPaymentMethodIndex.row];
        cardID = card.idCard;
        [Database updateCardDetailsForCardId:cardID andStatus:@"1"];
        [paymentTableView reloadData];
    }
}
-(void)tableView:(UITableView *)tableView1 didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    cell = [tableView1 cellForRowAtIndexPath:indexPath];
    if(indexPath.row == arrDBResult.count) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"walletHistory_off"]];
    } else {
        cell.accessoryView = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (BOOL)tableView:(UITableView *)tableView1 canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == arrDBResult.count)
        return NO;
    else
        return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle: (UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    Entity *card = arrDBResult[indexPath.row];
    [Database DeleteCard:card.idCard];
    arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
    [paymentTableView reloadData];
}

//    if(_isComingFromSummary == NO) {
//      if(arrDBResult.count == indexPath.row) {
//            [self addPayment];
//      } else {
//            CardDetails *fav = arrDBResult[indexPath.row];
//            NSMutableDictionary *getDetailsfromDB = [[NSMutableDictionary alloc]init];
//            [getDetailsfromDB setObject:fav.expMonth forKey:@"exp_month"];
//            [getDetailsfromDB setObject:fav.expYear forKey:@"exp_year"];
//            [getDetailsfromDB setObject:fav.cardtype forKey:@"type"];
//            [getDetailsfromDB setObject:fav.last4 forKey:@"last4"];
//            [getDetailsfromDB setObject:fav.idCard forKey:@"id"];
//            [self cardDetailsClicked:getDetailsfromDB];
//        }
//    } else {
//        if(arrDBResult.count == indexPath.row) {
//            CardLoginViewController *vc = (CardLoginViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"cardLogin"];
//            vc.isComingFromPayment = 2;
//            UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
//            [self presentViewController:navBar animated:YES completion:nil];
//        } else {
//            CardDetails *event = [arrDBResult objectAtIndex:indexPath.row];
//            NSString *cardId = event.idCard;
//            NSString *type   = event.cardtype;
//            NSString *last4 = event.last4;
//            if (callback) {
//                callback(cardId,type,last4);
//            }
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }
//    }

-(UIImage *)setPlaceholderToCardType:(NSString *)mycardType{
    NSString* cardTypeName;
    if([mycardType isEqualToString:@"amex"])
        cardTypeName = NSLocalizedString(@"amex", @"amex");
    else if([mycardType isEqualToString:@"diners"])
        cardTypeName = NSLocalizedString(@"diners", @"diners");
    else if([mycardType isEqualToString:@"discover"])
        cardTypeName = NSLocalizedString(@"discover", @"discover");
    else if([mycardType isEqualToString:@"jcb"])
        cardTypeName = NSLocalizedString(@"jcb", @"jcb");
    else if([mycardType isEqualToString:@"MasterCard"])
        cardTypeName = NSLocalizedString(@"mastercard", @"mastercard");
    else if([mycardType isEqualToString:@"Visa"])
        cardTypeName = NSLocalizedString(@"visa", @"visa");
    else
        cardTypeName = @"placeholder";
    return [UIImage imageNamed:cardTypeName];
}


-(void)addInDataBase {
    Database *db = [[Database alloc] init];
    [Database DeleteAllCard];
    for (int i =0; i<_arrayContainingCardInfo.count; i++) {
        NSMutableDictionary *dict = [_arrayContainingCardInfo[i] mutableCopy];
        if (i == 0) {
            [dict setObject:@"1" forKey:@"isDefault"];
        } else {
            [dict setObject:@"0" forKey:@"isDefault"];
        }
        [db makeDataBaseEntry:dict];
    }
}


- (void)checkCampaignIdAddedOrNot {
    isPresentInDBalready = 0;
    NSArray *array = [Database getCardDetails];
    if ([array count]) {
        for(int i=0 ; i<[array count];i++) {
            Entity *fav = [array objectAtIndex:i];
            if ([fav.idCard isEqualToString:_arrayContainingCardInfo[i][@"id"]]) {
                isPresentInDBalready = 1;
                break;
            }
        }
    }
}


#pragma mark - UIButton Actions -

- (IBAction)openHistoryAction:(id)sender {
    [self.view endEditing:YES];
    [self performSegueWithIdentifier:@"walletHistoryVC" sender:self];
}

- (IBAction)addMoneyBtn1Action:(id)sender {
    NSString *str = NSLocalizedString(@"100", @"100");
    addMoneytextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, str];
    [addMoneyBtn1 setSelected: YES];
    [addMoneyBtn2 setSelected: NO];
    [addMoneyBtn3 setSelected: NO];
    
}

- (IBAction)addMoneyBtn2Action:(id)sender {
    NSString *str = NSLocalizedString(@"200", @"200");
    addMoneytextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, str];
    [addMoneyBtn1 setSelected: NO];
    [addMoneyBtn2 setSelected: YES];
    [addMoneyBtn3 setSelected: NO];
}

- (IBAction)addMoneyBtn3Action:(id)sender {
    NSString *str = NSLocalizedString(@"300", @"300");
    addMoneytextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, str];
    [addMoneyBtn1 setSelected: NO];
    [addMoneyBtn2 setSelected: NO];
    [addMoneyBtn3 setSelected: YES];
}

- (IBAction)addCreditMoneyBtnAction:(id)sender {
    NSString *textFiledText = flStrForObj(addMoneytextField.text);
    NSString *amount = @"";
    NSRange replaceRange = [textFiledText rangeOfString:currencyStr];
    if (replaceRange.location != NSNotFound){
        amount = [textFiledText stringByReplacingCharactersInRange:replaceRange withString:@""];
    }
    NSInteger number = [amount integerValue];
    if (number == 0) {
        [Helper showAlertWithTitle:@"Message" Message:@"First enter amount"];
    }else if(number < 100) {
        NSString *minimumAmount = [PatientGetLocalCurrency getCurrencyLocal:[@"99" floatValue]];
        NSString *msg = [NSString stringWithFormat:@"The amount need to be more than %@", minimumAmount];
        [Helper showAlertWithTitle:@"Message" Message:msg];
    } else if(!arrDBResult.count){
        [Helper showAlertWithTitle:@"Message" Message:@"Please add your credit card first and then try again."];
    } else {
        
        [addMoneytextField resignFirstResponder];
        amountToAdd = amount;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message:NSLocalizedString(@"Are you sure that do you want to do transaction with above selected card?", @"Are you sure that do you want to do transaction with above selected card?") delegate:nil cancelButtonTitle:NSLocalizedString(@"Change", @"Change") otherButtonTitles:NSLocalizedString(@"Confirm", @"Confirm"), nil];
        alertView.delegate = self;
        alertView.tag = 1;
        [alertView show];
    }
}

#pragma mark - UIAlertView

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1 && buttonIndex == 1) {
        Entity *card = arrDBResult[defaultPaymentMethodIndex.row];
        [self addMoneyToWallet:amountToAdd withCardID:(NSString *)card.idCard];
    }
}

//PaymentViewController1 *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView1"];
//vc.callback = ^(NSString *cardIde , NSString *type, NSString *last4){
//    [self addMoneyToWallet:amountToAdd withCardID:(NSString *)cardIde];
//};
//vc.isComingFromSummary = YES;
//UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
//[self presentViewController:navBar animated:YES completion:nil];




#pragma mark - TextFields

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    activeTextField = textField;
    textField.text = currencyStr;
    [addMoneyBtn1 setSelected: NO];
    [addMoneyBtn2 setSelected: NO];
    [addMoneyBtn3 setSelected: NO];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text length] > 0) {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ([reachability isNetworkAvailable]) {
            if ([textField isEqual:addMoneytextField]) {
                NSLog(@"Call service to add Money into wallet");
            }
        } else {
            [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Network", @"No Network") On:self.view];
        }
    }
    self.activeTextField = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *typedText = textField.text;
    typedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (typedText.length <= currencyStr.length) {
        textField.text = currencyStr;
        return NO;
    } else if (typedText.length >= currencyStr.length+7) {
        [textField resignFirstResponder];
        return NO;
    }
    if (typedText.length) {
        NSString *amount;
        NSRange replaceRange = [typedText rangeOfString:currencyStr];
        if (replaceRange.location != NSNotFound){
            amount = [typedText stringByReplacingCharactersInRange:replaceRange withString:@""];
        }
        switch ([amount integerValue]) {
            case 100:
                [addMoneyBtn1 setSelected:YES];
                break;
            case 200:
                [addMoneyBtn2 setSelected:YES];
                break;
            case 300:
                [addMoneyBtn3 setSelected:YES];
                break;
            default:
                [addMoneyBtn1 setSelected: NO];
                [addMoneyBtn2 setSelected: NO];
                [addMoneyBtn3 setSelected: NO];
                break;
        }
    }
    return YES;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*--------------------------------------------*/
#pragma -marks Keyboard Appear and Hide Methods
/*--------------------------------------------*/
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification {
    tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.mainScrollView addGestureRecognizer:tap];
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [self.mainScrollView removeGestureRecognizer:tap];
    [self moveViewDown];
}
/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height {
    float textfieldMaxY = CGRectGetMaxY(textfield.frame) + 80;
    UIView *view = [textfield superview];
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height);
    if (remainder >= 0) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.mainScrollView.contentOffset = CGPointMake(0, -remainder);
                     }];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    self.mainScrollView.contentOffset = CGPointMake(0, 0);
    [self.mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height-44)];
    CGRect frame = self.mainScrollView.frame;
    frame.origin.y = 44;
    self.mainScrollView.frame = frame;
    
    
}


@end
