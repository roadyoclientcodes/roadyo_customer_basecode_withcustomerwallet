//
//  AccountViewController.h
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"
#import "RoundedImageView.h"


@interface AccountViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UploadFileDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    BOOL textFieldEditedFlag;
}
@property (weak, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;

//@property (strong, nonatomic) IBOutlet UITextField *passwordLabel;
@property (strong, nonatomic) IBOutlet UIImageView *accProfilePic;
@property (strong, nonatomic) IBOutlet UIButton *accProfileButton;

@property (strong, nonatomic) IBOutlet UITextField *accFirstNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *accLastNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *accEmailTextField;
@property (strong, nonatomic) IBOutlet UITextField *accPhoneNoTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneNoLbl;
@property (strong, nonatomic) UIActivityIndicatorView * activityIndicator;

@property (strong, nonatomic) UIImage *pickedImage;
//@property (strong, nonatomic) IBOutlet UIButton *pBtn;

- (IBAction)profilePicButtonClicked:(id)sender;
//- (IBAction)passwordButtonClicked:(id)sender;



@property (weak, nonatomic) IBOutlet UIView *languageView;
@property (strong, nonatomic) IBOutlet UILabel *chooseLanLabel;
@property (strong, nonatomic) IBOutlet UIButton *englsihLanBtn;
@property (strong, nonatomic) IBOutlet UIButton *arabicLanBtn;
- (IBAction)englishLanSelected:(id)sender;
- (IBAction)arabicLanSelected:(id)sender;

@end
