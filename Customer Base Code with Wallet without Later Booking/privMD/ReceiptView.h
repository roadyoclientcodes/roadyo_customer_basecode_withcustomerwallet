//
//  ReceiptView.h
//  RoadyoDispatch
//
//  Created by 3Embed on 25/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ReceiptView : UIView<UITableViewDelegate, UITableViewDataSource>
{
    NSDictionary *invoiceData;
    int serviceCalledCount;
}
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *receiptLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

- (IBAction)closeBtnAction:(id)sender;

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;

@property (nonatomic, copy)   void (^onCompletion)(NSInteger closeClicked);

@end
