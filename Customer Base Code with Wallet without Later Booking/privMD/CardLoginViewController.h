//
//  CardLoginViewController.h
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"

@interface CardLoginViewController : UIViewController<UITextFieldDelegate>
{
    BOOL checkMandatoryCard;
    BOOL checkMandatoryCamera;
    UITextField *postalText;
    int isPresentInDBalready;
}

@property (strong, nonatomic) IBOutlet UIButton *scanButton;
@property (strong, nonatomic)  UIButton *navNextButton;
@property (weak, nonatomic) IBOutlet UIView *paymentTextFieldBackgroundView;
@property (strong, nonatomic)  NSArray *getSignupDetails;
@property (strong,nonatomic)   NSArray *getInfoDetails;
@property (strong, nonatomic) UIImage *pickedImage;

//@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
//@property (strong, nonatomic) IBOutlet UILabel *cvvLabel;
//@property (strong, nonatomic) IBOutlet UILabel *postalLabel;
//@property (strong, nonatomic) IBOutlet UILabel *expLabel;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@property (assign,nonatomic) int isComingFromPayment;
@property (assign,nonatomic)  NSMutableArray *arrayContainingCardInfo;

- (IBAction)doneButtonClicked:(id)sender;


@end
