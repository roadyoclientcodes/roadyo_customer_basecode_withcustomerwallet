//
//  CardLoginViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CardLoginViewController.h"
#import "MapViewController.h"
#import "CardIO.h"
#import "UploadFiles.h"
#import "ViewController.h"
#import "Database.h"
#import "Entity.h"
#import <Stripe/Stripe.h>
#import "PatientViewController.h"

#define kPayPalEnvironment PayPalEnvironmentSandbox

@interface CardLoginViewController ()<CardIOPaymentViewControllerDelegate,STPPaymentCardTextFieldDelegate, UIAlertViewDelegate>
{
    STPPaymentCardTextField *paymentTextField;
    CGRect screenSize;

}
@end

@implementation CardLoginViewController

@synthesize navNextButton;
@synthesize getSignupDetails;
@synthesize scanButton;
@synthesize paymentTextFieldBackgroundView;
@synthesize doneButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg"]];
   
    [self setUpFrameForScanButton];
    
    screenSize = [[UIScreen mainScreen]bounds];
    
    //Setting Payment Textfield Properties
    [self setPaymentTextFieldProperties];
    [self.paymentTextFieldBackgroundView addSubview:paymentTextField];
}

-(void)setUpFrameForScanButton
{
//    CGRect frame = doneButton.frame;
//    frame.origin.y = 74;
//    doneButton.frame = frame;
//    
//    frame = paymentTextFieldBackgroundView.frame;
//    frame.origin.y = 74;
//    paymentTextFieldBackgroundView.frame = frame;
//    

    [doneButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    doneButton.titleLabel.font = [UIFont fontWithName:Roboto_Regular size:13];
    [scanButton setTitle:NSLocalizedString(@"Scan your card", @"Scan your card") forState:UIControlStateNormal];
    [scanButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [scanButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    scanButton.titleLabel.font = [UIFont fontWithName:Roboto_Regular size:13];
    [scanButton addTarget:self action:@selector(scanCardClicked:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setPaymentTextFieldProperties{
    
    paymentTextField = [[STPPaymentCardTextField alloc] init];
    
    paymentTextField.delegate = self;
    paymentTextField.cursorColor = [UIColor blueColor];
    paymentTextField.borderColor = [UIColor clearColor];
    paymentTextField.borderWidth = 0;
    paymentTextField.font = [UIFont fontWithName:Roboto_Bold size:12];
    paymentTextField.textColor = [UIColor blackColor];
    paymentTextField.cornerRadius = 2.0;
    
    paymentTextField.frame = CGRectMake(0, 0, paymentTextFieldBackgroundView.frame.size.width,paymentTextFieldBackgroundView.frame.size.height);
    
}


-(void)viewDidDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    if((_isComingFromPayment ==1)||(_isComingFromPayment == 2)) {
        if (_isComingFromPayment == 2)
        {
            CGRect frame = doneButton.frame;
            frame.origin.y = 7;
            doneButton.frame = frame;
            
            frame = paymentTextFieldBackgroundView.frame;
            frame.origin.y = 7;
            paymentTextFieldBackgroundView.frame = frame;
            
            frame = scanButton.frame;
            frame.origin.y = 60;
            scanButton.frame = frame;
        }
        self.navigationItem.title = NSLocalizedString(@"ADD CARD", @"ADD CARD");
        [self createNavLeftButton];
    } else {
        [self createNavView];
        [self createNavRightButton];
    }
}

-(void)createNavView {
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80, -10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0,0, 147, 30)];
    navTitle.text = NSLocalizedString(@"CREATE AN ACCOUNT", @"CREATE AN ACCOUNT");
    navTitle.textColor = UIColorFromRGB(0xffffff);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Roboto_Light size:13];
    [navView addSubview:navTitle];
    UIImageView *navImage = [[UIImageView alloc]initWithFrame:CGRectMake(0,30,147,7)];
    navImage.image = [UIImage imageNamed:@"signup_timer_third"];
    [navView addSubview:navImage];
    self.navigationItem.titleView = navView;
}

-(void)createNavRightButton {
    navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImage.size.width,buttonImage.size.height)];
    [navNextButton addTarget:self action:@selector(skipButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [Helper setButton:navNextButton Text:NSLocalizedString(@"SKIP", @"SKIP") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"SKIP", @"SKIP") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"SKIP", @"SKIP") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [navNextButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void) createNavLeftButton {
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 121 && buttonIndex == 1) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    }
}

-(void)skipButtonClicked {
    [self.view endEditing:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"You can edit the Card settings anytime from 'MY WALLET' in the Menu" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alert.tag = 121;
    [alert show];
}

- (IBAction)doneButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    
    [Stripe setDefaultPublishableKey:kPMDStripeTestKey];
    if (![paymentTextField isValid]) {
        [self showMessage:@"Please Enter Valid Card Details" andTitle:@"Message"];
        return;
    }
    if (![Stripe defaultPublishableKey]) {
        NSError *error = [NSError errorWithDomain:StripeDomain
                                             code:STPInvalidRequestError
                                         userInfo:@{
                                                    NSLocalizedDescriptionKey: @"Please specify a Stripe Publishable Key"
                                                    }];
        if (error)
        {
            [self showMessage:[error localizedDescription] andTitle:@"Message"];
        }
        return;
        
    }
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Adding Card...", @"Adding Card...")];
    
    //Creating token
    [[STPAPIClient sharedClient] createTokenWithCard:paymentTextField.cardParams
                                          completion:^(STPToken *token, NSError *error) {
                                              
                                              if (error) {
                                                  
                                                  [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                                  
                                                  [self showMessage:[error localizedDescription] andTitle:@"Message"];
                                              }
                                              else{
                                                  NSLog(@"%@",token.tokenId);
                                                                                                [self sendServiceAddCardsDetails:token.tokenId];
                                              }
                                              
                                          }];
}


-(void)showMessage:(NSString *)message andTitle:(NSString *)title {
    [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
}

-(void)cancelButtonClicked {
    if(_isComingFromPayment == 2) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - User Actions

- (void)scanCardClicked:(id)sender {
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:Roboto_Bold size:14], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [scanViewController.navigationBar setTitleTextAttributes:attributes];
    scanViewController.navigationBar.tintColor = [UIColor whiteColor];
    scanViewController.disableManualEntryButtons = YES;
    scanViewController.collectCVV = YES;
    [self presentViewController:scanViewController animated:YES completion:nil];
}

+ (BOOL)canReadCardWithCamera {
    return YES;
}


#pragma mark - CardIOPaymentViewControllerDelegate

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
    STPCardParams *cardParameters = [[STPCardParams alloc]init];
    cardParameters.number =  info.cardNumber;
    cardParameters.expMonth = info.expiryMonth;
    cardParameters.expYear = info.expiryYear;
    cardParameters.cvc = info.cvv;
    if([STPCardValidator validationStateForCard:cardParameters] ==  STPCardValidationStateValid){
        
        paymentTextField.cardParams = cardParameters;
        [self doneButtonClicked:nil];
        
    }
    else{
        
        [self showMessage:@"Please Enter Valid Card Details" andTitle:@"Message"];
        
    }
    
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - WebService call

-(void)sendServiceAddCardsDetails:(NSString *)token {
    NSString *deviceId;
    if (IS_SIMULATOR)
        deviceId = kPMDTestDeviceidKey;
    else
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    NSDictionary *params = @{
                             @"ent_sess_token":[[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken],
                             @"ent_dev_id":deviceId,
                             @"ent_token":token,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"addCard"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self addCardDetails:response];
                               }];
}

-(void)addCardDetails:(NSDictionary *)response
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    } else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    } else {
        NSDictionary *dictResponse= [response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
             _arrayContainingCardInfo = dictResponse[@"cards"];
            if (!_arrayContainingCardInfo || !_arrayContainingCardInfo.count){
            } else {
                [self addInDataBase];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
            }
            if(_isComingFromPayment == 2) {
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            } else if(_isComingFromPayment == 3) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1) {
            if(_isComingFromPayment == 2) {
                [[ProgressIndicator sharedInstance] hideProgressIndicator];
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                [self dismissViewControllerAnimated:YES completion:nil];
            } else if(_isComingFromPayment == 3) {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            } else {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

-(void)addInDataBase {
    Database *db = [[Database alloc] init];
    [Database DeleteAllCard];
    for (int i =0; i<_arrayContainingCardInfo.count; i++) {
        [db makeDataBaseEntry:_arrayContainingCardInfo[i]];
    }
}

- (void)checkCampaignIdAddedOrNot:(NSString *)cardId :(int)arrIndex
{
    isPresentInDBalready = 0;
    NSArray *array = [Database getCardDetails];
    if ([array count]== 0) {
    } else {
        for(int i=0 ; i<[array count];i++) {
            Entity *fav = [array objectAtIndex:i];
            if ([fav.idCard isEqualToString:_arrayContainingCardInfo[arrIndex][@"id"]]) {
                isPresentInDBalready = 1;
            }
        }
    }
}

@end
