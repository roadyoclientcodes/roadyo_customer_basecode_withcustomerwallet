//
//  BookingsViewController.h
//  UBER
//
//  Created by Rahul Sharma on 05/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property(nonatomic) NSInteger pageIndex;
@end
