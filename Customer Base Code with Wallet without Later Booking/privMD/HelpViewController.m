//
//  HelpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "HelpViewController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import <Canvas/CSAnimationView.h>
#import "UILabel+DynamicHeight.h"
#import <AVFoundation/AVFoundation.h>
#import "LocationServicesMessageVC.h"
#import "LanguageManager.h"
#import "Locale.h"

static NSBundle *bundle = nil;
@interface HelpViewController ()
{
    NSArray *typeO;
    NSArray *typeoDetails;
}

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end

@implementation HelpViewController
@synthesize topView;
@synthesize signInButton;
@synthesize registerButton;
@synthesize languageView;
@synthesize langaugeChangeBtn;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

-(void)locationServicesChanged:(NSNotification*)notification {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)//kCLAuthorizationStatusAuthorized
    {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController
{
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default-568h1"]];
    topView.frame = CGRectMake(0,568, 320,65);
    [self.view bringSubviewToFront:topView];
    [self animateIn];
    
    if (kLanguageSupport)
    {
        self.languageView.hidden = NO;
        [self.view bringSubviewToFront:self.languageView];
    }
    else
    {
        self.languageView.hidden = YES;
    }
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    Locale *localeObj;
    NSString *str;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_KEY_LANGUAGE_CODE] isEqualToString:kLangaugeCodeOtherThanEnglish])
    {
        localeObj = languageManager.availableLocales[1];
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"selectedLanguage"];
        str = @"ENGLISH";
        [self.langaugeChangeBtn setSelected:YES];
    }
    else
    {
        localeObj = languageManager.availableLocales[0];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"selectedLanguage"];
        str = @"عربي";
        [self.langaugeChangeBtn setSelected:NO];
    }
    [languageManager setLanguageWithLocale:localeObj];
    
    [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Roboto_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
   
    [Helper setButton:signInButton Text:NSLocalizedString(@"LOGIN", @"LOGIN") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0xfdd210) ShadowColor:nil];
    signInButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [signInButton setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateHighlighted];
    signInButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [signInButton setBackgroundImage:[UIImage imageNamed:@"loginSignUp_on"] forState:UIControlStateHighlighted];
    
    
    [Helper setButton:registerButton Text:NSLocalizedString(@"SIGN UP", @"SIGN UP") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    [registerButton setTitleColor:UIColorFromRGB(0xfdd210) forState:UIControlStateHighlighted];
    registerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    registerButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [registerButton setBackgroundImage:[UIImage imageNamed:@"loginSignUp_off"] forState:UIControlStateHighlighted];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
}

- (void)animateIn
{
    CGRect frameTopview = topView.frame;
    frameTopview.origin.y = self.view.bounds.size.height - 65;;
    [UIView animateWithDuration:0.6f animations:^{
        topView.frame = frameTopview;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
}

-(void) viewDidDisappear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)signInButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"SignIn" sender:self];
}

- (IBAction)registerButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"SignUp" sender:self];
}


- (IBAction)langugaeChange:(id)sender
{
    if (self.langaugeChangeBtn.isSelected)
    {
        [self.langaugeChangeBtn setSelected:NO];
        NSString *str = @"العربية";
        [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Roboto_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"selectedLanguage"];
        LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
        Locale *localeObj = languageManager.availableLocales[0];
        [languageManager setLanguageWithLocale:localeObj];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [self updateUI];
    }
    else
    {
        [self.langaugeChangeBtn setSelected:YES];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default-568h2"]];
        NSString *str = @"ENGLISH";
        [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Roboto_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"selectedLanguage"];
        LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
        Locale *localeObj = languageManager.availableLocales[1];
        [languageManager setLanguageWithLocale:localeObj];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [self updateUI];
    }
}

- (IBAction)rmsStoreButtonClicked:(id)sender
{
}

- (IBAction)gpsTrackingButtonClicked:(id)sender
{
}

- (IBAction)rentaCarButtonClicked:(id)sender
{
}

-(void) updateUI
{
    [Helper setButton:signInButton Text:NSLocalizedString(@"LOGIN", @"LOGIN") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [Helper setButton:registerButton Text:NSLocalizedString(@"SIGN UP", @"SIGN UP") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    
//    if ([UIApplication sharedApplication].userInterfaceLayoutDirection != UIUserInterfaceLayoutDirectionRightToLeft &&[[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_KEY_LANGUAGE_CODE] isEqualToString:kLangaugeCodeOtherThanEnglish])
//    {
//        [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
//    }
//    else
//    {
//        [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
//    }

}

@end
