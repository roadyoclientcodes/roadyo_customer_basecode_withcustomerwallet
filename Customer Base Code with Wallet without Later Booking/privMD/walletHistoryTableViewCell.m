//
//  walletHistoryTableViewCell.m
//  RoadyoDispatch
//
//  Created by 3Embed on 23/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "walletHistoryTableViewCell.h"

@implementation walletHistoryTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)updateCellUI:(NSDictionary *)dict {
    NSString *balanceAmount = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dict[@"amount"]) floatValue]];
    if ([flStrForStr(dict[@"type"]) integerValue] == 1) {
        self.paymentStatusImageView.image = [UIImage imageNamed:@"down_arrow_icon"];
        self.walletBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",balanceAmount, NSLocalizedString(@"PQ Money Balance", @"PQ Money Balance")];
        self.paymentReasonLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"PQ Money Recharge: ", @"PQ Money Recharge: "), flStrForStr(dict[@"txn_id"])];
    } else {
        self.paymentStatusImageView.image = [UIImage imageNamed:@"up_arrow_icon"];
        self.walletBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",balanceAmount, NSLocalizedString(@"PQ Money Balance", @"PQ Money Balance")];
        self.paymentReasonLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"PQ Booking", @"PQ Booking")];
    }
    self.dateLabel.text = [self getDate:flStrForStr(dict[@"date"])];
    self.amountLable.text = balanceAmount;
}

-(NSString*)getDate:(NSString *)aDatefromServer {
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EE, d LLLL yyyy hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

@end
