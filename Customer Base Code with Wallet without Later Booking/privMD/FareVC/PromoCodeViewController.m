//
//  PromoCodeViewController.m
//  CabRyderPassenger
//
//  Created by Rahul Sharma on 23/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "PromoCodeViewController.h"

@interface PromoCodeViewController ()
@property (strong, nonatomic) IBOutlet UITextField *promoCodeTextField;
- (IBAction)textEditingEnds:(id)sender;

@end

@implementation PromoCodeViewController
@synthesize onCompletion;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
    [self setTitle:NSLocalizedString(@"PROMO CODE", @"PROMO CODE")];
    [self createNavLeftButton];
    [_promoCodeTextField becomeFirstResponder];
    
    UITapGestureRecognizer *dismissKeyBoard = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
    
    [self.view addGestureRecognizer:dismissKeyBoard];
}

-(void)viewWillDisappear:(BOOL)animated {
 
    
}
-(void)dismiss{
    
    [self.view endEditing:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - WEB SERVICES

-(void)sendAServiceForPromoCode:(NSString *)code {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_coupon":[NSString stringWithFormat:@"%@",code],
                             @"ent_lat":[NSNumber numberWithDouble:_userLatitude],
                             @"ent_long":[NSNumber numberWithDouble:_userLongitude],
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };

    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"checkCoupon"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *responseDictionary){
                                   
                                   if (success)
                                   {
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];

                                       if (!responseDictionary)
                                       {
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[responseDictionary objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                                           [alertView show];
                                           
                                       }
                                       else if ([responseDictionary objectForKey:@"Error"])
                                       {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDictionary objectForKey:@"Error"]];
                                       }
                                       else
                                       {
                                           if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
                                           {
                                               if (onCompletion) {
                                                   
                                                   onCompletion(code);
                                               }
                                               
                                           }
                                           else
                                           {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
                                           }
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }
                                   }
                                   else
                                   {
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                   }
                               }];
    
    
}



#pragma mark - CustomNavigation Button

-(void)cancelButtonClicked
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void) createNavLeftButton
{
    // UIView *navView = [[UIView new]initWithFrame:CGRectMake(0, 0,50, 44)];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
   
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

- (IBAction)textEditingEnds:(id)sender {
    
    
}

#pragma mark-
#pragma UITextFields Delegate

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField.text.length > 0) {
        
        [self sendAServiceForPromoCode:textField.text];
    }
    
    return YES;
}

@end
