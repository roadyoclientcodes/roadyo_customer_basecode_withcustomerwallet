//
//  PaymentViewController.h
//  privMD
//
//  Created by Rahul Sharma on 02/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ChooseCardCallback)(NSString *cardId , NSString *type, NSString *last4);

@interface PaymentViewController : UIViewController <UITableViewDataSource,UITableViewDelegate> {
    BOOL isGoingDelete;
    int isPresentInDBalready;
    PatientAppDelegate *appDelegate;
    NSMutableArray		*arrDBResult;
    NSManagedObjectContext *context;
}

@property (nonatomic,copy) ChooseCardCallback callback;
@property (assign,nonatomic) BOOL isComingFromSummary;
@property (strong,nonatomic) NSMutableArray *arrayContainingCardInfo;
@property (strong,nonatomic) NSDictionary *dict;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITableView *paymentTableView;
@property (weak, nonatomic) IBOutlet UIView *walletView;
@property (weak, nonatomic) IBOutlet UIView *walletHeadingView;
@property (weak, nonatomic) IBOutlet UILabel *walletHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentBalanceStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentBalanceAmountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *nextArrow;
- (IBAction)openHistoryAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *addMoneytextField;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn1;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn2;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn3;

- (IBAction)addMoneyBtn1Action:(id)sender;
- (IBAction)addMoneyBtn2Action:(id)sender;
- (IBAction)addMoneyBtn3Action:(id)sender;
- (IBAction)addCreditMoneyBtnAction:(id)sender;

@end
