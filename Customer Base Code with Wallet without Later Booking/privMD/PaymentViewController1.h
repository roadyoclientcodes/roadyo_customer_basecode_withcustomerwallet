//
//  PaymentViewController1.h
//  privMD
//
//  Created by Rahul Sharma on 02/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^chooseCard)(NSString *cardId , NSString *type, NSString *last4);

@interface PaymentViewController1 : UIViewController <UITableViewDataSource,UITableViewDelegate> {
    BOOL isGoingDelete;
    int isPresentInDBalready;
    PatientAppDelegate *appDelegate;
    NSMutableArray		*arrDBResult;
	NSManagedObjectContext *context;
}

@property (nonatomic,copy) chooseCard callback;
@property (assign,nonatomic) BOOL isComingFromSummary;
@property (strong,nonatomic) NSMutableArray *arrayContainingCardInfo;
@property (strong,nonatomic) NSDictionary *dict;
@property (weak, nonatomic) IBOutlet UITableView *paymentTable;

@end
