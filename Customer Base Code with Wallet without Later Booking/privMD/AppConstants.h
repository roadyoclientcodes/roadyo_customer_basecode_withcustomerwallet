//
//  AppConstants.h
//  privMD
//
//  Created by Surender Rathore on 22/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//




#pragma mark - enums

typedef enum {
    
    kNotificationTypeBookingAccept = 2,
    kNotificationTypeBookingOnMyWay = 6,
    kNotificationTypeBookingReachedLocation = 7,
    kNotificationTypeBookingStarted = 8,
    kNotificationTypeBookingComplete = 9,
    kNotificationTypeBookingReject = 10,
    kNotificationTypePassengerRejected = 12,
    kNotificationTypePassengerRecieveMessage = 420
    
}BookingNotificationType;

typedef enum CancelBookingReasons :NSUInteger{
    
    kCAPassengerDoNotShow = 4,
    kCAWrongAddressShown = 5,
    kCAPassengerRequestedCancel = 6,
    kCADoNotChargeClient = 7,
    kCAOhterReasons = 8,
    
}CancelBookingReasons;

typedef enum {
    kPubNubStartStreamAction = 1,
    kPubNubStopStreamAction = 2,
    kPubNubUpdatePatientAppointmentLocationAction = 3,
    kPubNubStartDoctorLocationStreamAction = 5,
    kPubNubStopDoctorLocationStreamAction  = 6
}PubNubStreamAction;


typedef enum {
    kAppointmentTypeNow = 3,
    kAppointmentTypeLater = 4
}AppointmentType;

typedef enum {
    kSourceAddress = 1,
    kDestinationAddress = 2,
    kUnknownAddress = 3
}AddressTypeForPickUpLocation;


#pragma mark - Constants
//eg: give prifix kPMD

extern NSString *const kPMDPublishStreamChannel;
extern NSString *const kPMDPubNubPublisherKey;
extern NSString *const kPMDPubNubSubcriptionKey;
extern NSString *const kPMDGoogleMapsAPIKey;
extern NSString *const kPMDServerGoogleMapsAPIKey;
extern NSString *const kPMDFlurryId;
extern NSString *const kPMDTestDeviceidKey;
extern NSString *const kPMDDeviceIdKey;
extern NSString *const KUBERCarArrayKey;
extern NSString *const kPMDStripeTestKey;
extern NSString *const kPMDStripeLiveKey;
//extern NSString *const kNSUDefaultpaymentType;

#pragma mark - mark URLs

//Base URL

extern NSString *BASE_URL;
extern NSString *BASE_URL_SUPPORT;
extern NSString *BASE_URL_UPLOADIMAGE;
extern NSString *const   baseUrlForXXHDPIImage;
extern NSString *const   baseUrlForOriginalImage;

extern NSString *const termsAndCondition;
extern NSString *const privacyPolicy;
extern NSString *const itunesURL;
extern NSString *const facebookURL;
extern NSString *const websiteLabel;
extern NSString *const websiteURL;
extern NSString *const imgLinkForSharing;

//Methods
extern NSString *MethodPatientSignUp;
extern NSString *MethodPatientLogin;
extern NSString *MethodDoctorUploadImage;
extern NSString *MethodPassengerLogout;
extern NSString *MethodFareCalculator;


#pragma mark - ServiceMethods
// eg : prifix kSM
extern NSString *const kSMLiveBooking;
extern NSString *const kSMGetAppointmentDetial;
extern NSString *const kSMCancelAppointment;
extern NSString *const kSMCancelOngoingAppointment;
extern NSString *const kSMUpdateSlaveReview;
extern NSString *const kSMGetMasters ;


#pragma marks - Distance Km or Miles
extern double   const kPMDDistanceMetric;
extern NSString *const kPMDDistanceParameter;
extern NSString *const kPMDMPHorKMPH;

#pragma marks - Langauge Support 
extern BOOL const kLanguageSupport;
extern BOOL const kRTLSupport;
extern NSString *const DEFAULTS_KEY_LANGUAGE_CODE;
extern NSString *const kLangaugeCodeOtherThanEnglish;

extern BOOL const kPMDPaymentType;
extern BOOL const kPMDCardOrCash;
extern BOOL const kPMDBookLater;
extern BOOL const kPMDWithDispatch;


//Request Params For SignUp
extern NSString *KDASignUpFirstName;
extern NSString *KDASignUpLastName;
extern NSString *KDASignUpMobile;
extern NSString *KDASignUpEmail;
extern NSString *KDASignUpPassword;
extern NSString *KDASignUpCountry;
extern NSString *KDASignUpCity;
extern NSString *KDASignUpLanguage;
extern NSString *KDASignUpDeviceType;
extern NSString *KDASignUpDeviceId;
extern NSString *KDASignUpAddLine1;
extern NSString *KDASignUpAddLine2;
extern NSString *KDASignUpPushToken;
extern NSString *KDASignUpZipCode;
extern NSString *KDASignUpAccessToken;
extern NSString *KDASignUpDateTime;
extern NSString *KDASignUpCreditCardNo;
extern NSString *KDASignUpCreditCardCVV;
extern NSString *KDASignUpCreditCardExpiry;
extern NSString *KDASignUpTandC;
extern NSString *KDASignUpPricing;
extern NSString *KDASignUpReferralCode;
extern NSString *KDASignUpLatitude;
extern NSString *KDASignUpLongitude;


//Request Params For Login

extern NSString *KDALoginEmail;
extern NSString *KDALoginPassword;
extern NSString *KDALoginDeviceType;
extern NSString *KDALoginDevideId;
extern NSString *KDALoginPushToken;
extern NSString *KDALoginUpDateTime;

//Request for Upload Image
extern NSString *KDAUploadDeviceId;
extern NSString *KDAUploadSessionToken;
extern NSString *KDAUploadImageName;
extern NSString *KDAUploadImageChunck;
extern NSString *KDAUploadfrom;
extern NSString *KDAUploadtype;
extern NSString *KDAUploadDateTime;
extern NSString *KDAUploadOffset;

//Request Params For Logout the user

extern NSString *KDALogoutSessionToken;
extern NSString *KDALogoutUserId;
extern NSString *KDALogoutDateTime;

//Parsms for checking user loged out or not

extern NSString *KDAcheckUserLogedOut;
extern NSString *KDAcheckUserSessionToken;
extern NSString *KDAgetPushToken;

//Params to store the Country & City.

extern NSString *KDACountry;
extern NSString *KDACity;
extern NSString *KDALatitude;
extern NSString *KDALongitude;

//params for firstname
extern NSString *KDAFirstName;
extern NSString *KDALastName;
extern NSString *KDAEmail;
extern NSString *KDAPhoneNo;
extern NSString *KDAPassword;


#pragma mark - NSUserDeafults Keys
//eg : give prefix kNSU
extern NSString *const kNSUMongoDataBaseAPIKey;
extern NSString *const kNSUIsPassengerBookedKey;
extern NSString *const kNSUPassengerBookingStatusKey;
extern NSString *const kNSUPatientPaypalkey;


extern NSString *const KNSUPubnubPublishKey;
extern NSString *const kNSUPubnubSubscribeKey;

extern NSString *const kNSUUserPubnubChannel;
extern NSString *const kNSUServerPubnubChannel;
extern NSString *const kNSUPresensePubnubChannel;
extern NSString *const kNSUDriverPubnubChannel;

extern NSString *const KNSUSid;
extern NSString *const kNSUUserEmailID;
extern NSString *const kNSUUserReferralCode;
extern NSString *const kNSUUserShareMessage;

extern NSString *const kNSUPlaceAPIKey;
extern NSString *const kNSUStripeKey;


#pragma mark - PushNotification Payload Keys
//eg : give prefix kPN
extern NSString *const kPNPayloadDoctorNameKey;
extern NSString *const kPNPayloadAppoinmentTimeKey;
extern NSString *const kPNPayloadDistanceKey;
extern NSString *const kPNPayloadEstimatedTimeKey;
extern NSString *const kPNPayloadDoctorEmailKey;
extern NSString *const kPNPayloadDoctorContactNumberKey;
extern NSString *const kPNPayloadProfilePictureUrlKey;
extern NSString *const kPNPayloadStatusIDKey;
extern NSString *const kPNPayloadAppointmentIDKey;
extern NSString *const kPNPayloadStatusIDKey;



#pragma mark - Controller Keys

#pragma mark - Notification Name keys
extern NSString *const kNotificationNewCardAddedNameKey;
extern NSString *const kNotificationCardDeletedNameKey;
extern NSString *const kNotificationLocationServicesChangedNameKey;
extern NSString *const kNotificationBookingConfirmationNameKey;
#pragma mark - Network Error
extern NSString *const kNetworkErrormessage;

extern NSString *const KNUCurrentLat;
extern NSString *const KNUCurrentLong;
extern NSString *const KNUserCurrentCity;
extern NSString *const KNUserCurrentState;
extern NSString *const KNUserCurrentCountry;

extern NSString *const KUDriverEmail;
extern NSString *const KUBookingDate;
extern NSString *const KUBookingID;


