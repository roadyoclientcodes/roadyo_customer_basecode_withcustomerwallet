//
//  HelpViewController.h
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CSAnimationView;
@interface HelpViewController : UIViewController<UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIButton *signInButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *langaugeChangeBtn;
@property (weak, nonatomic) IBOutlet UIView *languageView;
@property (weak, nonatomic) IBOutlet UIButton *rmsStoreButtonClicked;
- (IBAction)signInButtonClicked:(id)sender;
- (IBAction)registerButtonClicked:(id)sender;
- (IBAction)langugaeChange:(id)sender;
- (IBAction)rmsStoreButtonClicked:(id)sender;
- (IBAction)gpsTrackingButtonClicked:(id)sender;
- (IBAction)rentaCarButtonClicked:(id)sender;


@end
