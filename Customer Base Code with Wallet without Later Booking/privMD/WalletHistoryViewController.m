//
//  WalletHistoryViewController.m
//  RoadyoDispatch
//
//  Created by 3Embed on 23/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "WalletHistoryViewController.h"
#import "walletHistoryTableViewCell.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"

@interface WalletHistoryViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, CustomNavigationBarDelegate> {
    int indexNumber;
    NSMutableArray *walletAllHistory;
    NSMutableArray *walletInHistory;
    NSMutableArray *walletOutHistory;
}

@end

@implementation WalletHistoryViewController
@synthesize tableBackgroundScrollView;
@synthesize allHistoryTableView, moneyInTableView, moneyOutTableView;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self addCustomNavigationBar];
    walletAllHistory = [[NSMutableArray alloc] init];
    walletInHistory = [[NSMutableArray alloc] init];
    walletOutHistory = [[NSMutableArray alloc] init];
    [Helper setButton:self.allBtn Text:NSLocalizedString(@"All", @"All")  WithFont:Lato_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [Helper setButton:self.moneyInBtn Text:NSLocalizedString(@"Money - In", @"Money - In")  WithFont:Lato_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [Helper setButton:self.moneyOutBtn Text:NSLocalizedString(@"Money - Out", @"Money - Out")  WithFont:Lato_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    tableBackgroundScrollView.frame = CGRectMake(0, 0, screenWidth, screenHeight-self.footerView.frame.origin.y);
    [tableBackgroundScrollView setContentSize:CGSizeMake(screenWidth*3, screenHeight-self.footerView.frame.origin.y)];
    allHistoryTableView.frame = CGRectMake(0, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
    moneyInTableView.frame = CGRectMake(screenWidth, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
    moneyOutTableView.frame = CGRectMake(screenWidth*2, 0, screenWidth, tableBackgroundScrollView.frame.size.height);
    tableBackgroundScrollView.contentOffset = CGPointMake(0,0);
    indexNumber = 0;
    [self getWalletHistory:[NSString stringWithFormat:@"%d", indexNumber]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"WALLET MONEY HISTORY", @"WALLET MONEY HISTORY")];
    [customNavigationBarView hideLeftMenuButton:YES];
    [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"BACK", @"BACK")];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender {
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton = NO;
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UIButton Action - 

- (IBAction)allBtnAction:(id)sender {
    CGRect frame = tableBackgroundScrollView.bounds;
    frame.origin.x = 0 * CGRectGetWidth(self.view.frame);
    [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
}

- (IBAction)moneyInBtnAction:(id)sender {
    CGRect frame = tableBackgroundScrollView.bounds;
    frame.origin.x = 1 * CGRectGetWidth(self.view.frame);
    [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
}

- (IBAction)moneyOutBtnAction:(id)sender {
    CGRect frame = tableBackgroundScrollView.bounds;
    frame.origin.x = 2 * CGRectGetWidth(self.view.frame);
    [tableBackgroundScrollView scrollRectToVisible:frame animated:YES];
}

#pragma mark - UITableView DataSource - 

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == allHistoryTableView || tableView == moneyInTableView || tableView == moneyOutTableView) {
        return 1;
    } else {
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == allHistoryTableView) {
        return  walletAllHistory.count;
    } else if (tableView == moneyInTableView) {
        return walletInHistory.count;
    } else if (tableView == moneyOutTableView) {
        return  walletOutHistory.count;
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 86;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict;
    if (tableView == allHistoryTableView) {
        dict =  walletAllHistory[indexPath.row];
    } else if (tableView == moneyInTableView) {
        dict =  walletInHistory[indexPath.row];
    } else if (tableView == moneyOutTableView) {
        dict =  walletOutHistory[indexPath.row];
    }
    walletHistoryTableViewCell *cell = (walletHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"walletHistoryCell"];
    [cell updateCellUI:dict];
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == tableBackgroundScrollView) {
        CGFloat scrollX = scrollView.contentOffset.x;
        CGRect sliderFrame = self.slider.frame;
        sliderFrame.origin.x = scrollX/3;
        self.slider.frame = sliderFrame;
    }
}


#pragma mark - WebServices -

-(void) getWalletHistory:(NSString *)pageIndex {
    NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
    NSString *sessionToken = [udPlotting objectForKey:KDAcheckUserSessionToken];
    NSString *sid = flStrForObj([udPlotting objectForKey:KNSUSid]);
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
        deviceID = kPMDTestDeviceidKey;
    else
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_sid":flStrForObj(sid),
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_page_index":pageIndex
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getWalletHistory"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       indexNumber++;
                                       [self getWalletHistoryResponse:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

-(void) getWalletHistoryResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict objectForKey:@"Error"]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDict objectForKey: @"Error"]];
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        walletAllHistory =  [responseDict[@"walletHistory"] mutableCopy];
        for (NSDictionary *dict  in walletAllHistory) {
            if ([flStrForStr(dict[@"type"]) integerValue] == 1) {
                [walletInHistory addObject:dict];
            } else if ([flStrForStr(dict[@"type"]) integerValue] == 2) {
                [walletOutHistory addObject:dict];
            }
        }
        [self.moneyInTableView reloadData];
        [self.moneyOutTableView reloadData];
        [self.allHistoryTableView reloadData];
    }
}


@end
