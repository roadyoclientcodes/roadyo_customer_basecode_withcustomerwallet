//
//  InvoiceView.m
//  RoadyoDispatch
//
//  Created by 3Embed on 24/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "InvoiceView.h"
#import "ReceiptView.h"
#import "MessageView.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import <AXRatingView/AXRatingView.h>

@implementation InvoiceView
@synthesize driverEmail;
@synthesize apptDate;

@synthesize yourLastRideLabe, dayDateLabel, needHelpBtn;
@synthesize pickUpWithDateLabel, pickUpAddressLabel, dropOffWithDateLabel, dropOffAddressLabel;
@synthesize greenDotLabel, redDotLabel, verticalLineLabel, horizontalLineLabel;
@synthesize driverImageView, driverNameLabel, totalAmountLabel, receiptBtn;
@synthesize rateYourDriverLabel, ratingView;
@synthesize writeCommentBtn;
@synthesize submitBtn;
@synthesize isComingFromBookingHistory;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"InvoiceView" owner:self options:nil] firstObject];
    return self;
}

- (IBAction)receiptBtnAction:(id)sender
{
    ReceiptView* receiptView = [[ReceiptView alloc]init];
    [receiptView showPopUpWithDetailedDict:invoiceData Onwindow:window];
    receiptView.onCompletion = ^(NSInteger closeClicked)
    {
        [self setHidden:NO];
    };
    [self setHidden:YES];
}

- (IBAction)writeCommentBtnAction:(id)sender
{
    MessageView* messageView = [[MessageView alloc]init];
    messageView.headingText = NSLocalizedString(@"LEAVE A COMMENT", @"LEAVE A COMMENT");
    messageView.textViewPlaceHolder = NSLocalizedString(@"Let us know your experience on this last trip and we will follow up with our team to improve your experience in the next time you take a ride", @"Let us know your experience on this last trip and we will follow up with our team to improve your experience in the next time you take a ride");
    messageView.isDispute = NO;
    [messageView showPopUpWithDetailedDict:nil Onwindow:window];
    messageView.onCompletion = ^(NSInteger closeClicked, NSString *reportMsg)
    {
        if (closeClicked == 1)
        {
            textWritenByUser = reportMsg;
        }
        else
        {
            textWritenByUser = @"";
        }
        [self setHidden:NO];
    };
    [self setHidden:YES];
}

- (IBAction)needHelpBtnAction:(id)sender
{
    MessageView* messageView = [[MessageView alloc]init];
    messageView.headingText = NSLocalizedString(@"NEED HELP?", @"NEED HELP?");
    messageView.textViewPlaceHolder = NSLocalizedString(@"Please let us know if you faced any issue on this ride ?", @"Please let us know if you faced any issue on this ride ?");
    messageView.isDispute = YES;
    messageView.appoinmentDate = invoiceData[@"apptDt"];
    [messageView showPopUpWithDetailedDict:nil Onwindow:window];
    messageView.onCompletion = ^(NSInteger closeClicked, NSString *reportMsg)
    {
        [self setHidden:NO];
    };
    [self setHidden:YES];
}

- (IBAction)submitBtnAction:(id)sender
{
    if (isComingFromBookingHistory)
    {
        [self hidePOPup];
    }
    else
    {
        [self sendRequestForReviewSubmit];
    }
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict
{
    window = [[UIApplication sharedApplication] keyWindow];
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    
    [self setRatingViewInInvoice];
    [self sendRequestForAppointmentInvoice];
    
    if (isComingFromBookingHistory)
    {
        [Helper setButton:submitBtn Text:NSLocalizedString(@"OK", @"OK") WithFont:Lato_Regular FSize:14 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
        self.needHelpBtn.hidden = YES;
        self.ratingViewHeight.constant = 0;
        self.commentBtnHeight.constant = 0;
        [self layoutIfNeeded];
    }
    else
    {
        self.needHelpBtn.hidden = NO;
        [Helper setToLabel:rateYourDriverLabel Text:NSLocalizedString(@"RATE YOUR DRIVER", @"RATE YOUR DRIVER") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
        [Helper setButton:writeCommentBtn Text:NSLocalizedString(@"LEAVE A COMMENT", @"LEAVE A COMMENT") WithFont:Lato_Regular FSize:13 TitleColor:UIColorFromRGB(0xd0d0d0) ShadowColor:nil];
        [Helper setButton:submitBtn Text:NSLocalizedString(@"SUBMIT", @"SUBMIT") WithFont:Lato_Regular FSize:14 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
    }
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hidePOPup
{
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 0.5;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

-(void)sendRequestForAppointmentInvoice
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    NSString *str = NSLocalizedString(@"Please wait...", @"Please wait...");
    [pi showPIOnView:self withMessage:str];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSInteger tip = [[NSUserDefaults standardUserDefaults]integerForKey:@"drivertip"];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *docEmail = driverEmail;
    NSString *appointmntDate = apptDate;
    
    NSString *currentDate = [Helper getCurrentDateTime];
    @try
    {
        NSDictionary *params = @{@"ent_sess_token":sessionToken,
                                 @"ent_dev_id":deviceID,
                                 @"ent_email":docEmail,
                                 @"ent_user_type":@"2",
                                 @"ent_tip":[NSNumber numberWithInteger:tip],
                                 @"ent_appnt_dt":appointmntDate,
                                 @"ent_date_time":currentDate,
                                 };
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success)
                                       {
                                           [self parseAppointmentDetailResponse:response];
                                       }
                                       else
                                       {
                                           [self sendRequestForAppointmentInvoice];
                                       }
                                   }];
    }
    @catch (NSException *exception)
    {
    }
}

#pragma mark - WebService Response
-(void)parseAppointmentDetailResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 ))
    {
        //session Expired
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            invoiceData = [response mutableCopy];
            [self updateUI];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:@"errMsg"];
//            [self hidePOPup];
        }
    }
}

-(void)updateUI
{
    NSString *rideDate = [self getDateAndTime:flStrForObj(invoiceData[@"dropDt"])];
    [Helper setToLabel:dayDateLabel Text:rideDate WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x919191)];
    
    [Helper setToLabel:yourLastRideLabe Text:NSLocalizedString(@"YOUR LAST RIDE", @"YOUR LAST RIDE") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    
    [Helper setButton:receiptBtn Text:NSLocalizedString(@"RECEIPT", @"RECEIPT") WithFont:Lato_Regular FSize:12 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    
    [Helper setButton:self.needHelpBtn Text:NSLocalizedString(@"NEED HELP?", @"NEED HELP?") WithFont:Lato_Regular FSize:12 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
   
    greenDotLabel.layer.cornerRadius = greenDotLabel.frame.size.width/2;
    greenDotLabel.layer.masksToBounds = YES;
    
    redDotLabel.layer.cornerRadius = redDotLabel.frame.size.width/2;
    redDotLabel.layer.masksToBounds = YES;
    
    
    NSString *pickUp = NSLocalizedString(@"PICK UP", @"PICK UP");
    NSString *pTime = [self getTime:flStrForObj(invoiceData[@"pickupDt"])];
    [Helper setToLabel:pickUpWithDateLabel Text:[NSString stringWithFormat:@"%@ %@", pickUp,pTime] WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:pickUpAddressLabel Text:flStrForObj(invoiceData[@"addr1"]) WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x6f6f6f)];
    
    NSString *dropOff = NSLocalizedString(@"DROP OFF", @"DROP OFF");
    NSString *dTime = [self getTime:flStrForObj(invoiceData[@"dropDt"])];
    [Helper setToLabel:dropOffWithDateLabel Text:[NSString stringWithFormat:@"%@ %@", dropOff,dTime] WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:dropOffAddressLabel Text:flStrForObj(invoiceData[@"dropAddr1"]) WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x6f6f6f)];
    
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    NSString *strImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,flStrForObj(invoiceData[@"pPic"])];
    [driverImageView sd_setImageWithURL:[NSURL URLWithString:strImageURL]
                       placeholderImage:nil
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                  
                                  driverImageView.image = image;
                              }];
    
    [Helper setToLabel:driverNameLabel Text:[[NSString stringWithFormat:@"%@ %@",flStrForObj(invoiceData[@"fName"]),flStrForObj(invoiceData[@"lName"])] uppercaseString] WithFont:Lato_Regular FSize:15 Color:UIColorFromRGB(0x707070)];
    
    NSString *totalAmount =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
    [Helper setToLabel:totalAmountLabel Text:totalAmount WithFont:Lato_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
    
    needHelpBtn.layer.cornerRadius = 4.0f;
    needHelpBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    needHelpBtn.layer.borderWidth = 1.0f;
    
    receiptBtn.layer.cornerRadius = 4.0f;
    receiptBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    receiptBtn.layer.borderWidth = 1.0f;
    
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    
    self.writeCommentBtn.layer.cornerRadius = 5.0f;
    self.writeCommentBtn.layer.masksToBounds = YES;

    self.submitBtn.layer.cornerRadius = 5.0f;
    self.submitBtn.layer.masksToBounds = YES;

    if (isComingFromBookingHistory)
    {
        ratingView.value = [invoiceData[@"r"] floatValue];
        ratingView.userInteractionEnabled = NO;
    }
    else
    {
        [ratingView addTarget:self action:@selector(ratingChanged:) forControlEvents:UIControlEventValueChanged];
        ratingView.value = 5.0;
        ratingView.userInteractionEnabled = YES;
    }
}

-(void)sendRequestForReviewSubmit
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self withMessage:NSLocalizedString(@"Submitting...", @"Submitting...")];
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *docEmail = invoiceData[@"email"];
    NSString *appointmntDate = invoiceData[@"apptDt"];
    NSString *currentDate = [Helper getCurrentDateTime];

    NSString *review;
    if (textWritenByUser)
    {
        review = textWritenByUser;
    }
    else
    {
        review = @"";
    }
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":docEmail,
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             @"ent_rating_num":[NSNumber numberWithInteger:ratingValue],
                             @"ent_review_msg":review,
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMUpdateSlaveReview
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self parseSubmitReviewResponse:response];
                                   }
                               }];
}

-(void)parseSubmitReviewResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
            [self hidePOPup];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"errMsg"]];
        }
    }
}


-(void) setRatingViewInInvoice
{
    ratingView.stepInterval = 1.0;
    ratingView.markFont = [UIFont systemFontOfSize:30];
    ratingView.baseColor = [UIColor colorWithRed:0.293 green:0.303 blue:0.322 alpha:1.000];
    ratingView.highlightColor = [UIColor colorWithRed:0.975 green:0.736 blue:0.056 alpha:1.000];
}

- (void)ratingChanged:(AXRatingView *)sender
{
    ratingValue = sender.value;
}


-(NSString*)getTime:(NSString *)aDatefromServer
{
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    NSLocale *locale;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_KEY_LANGUAGE_CODE] isEqualToString:kLangaugeCodeOtherThanEnglish])
    {
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar"];
    }
    else
    {
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"us"];
    }
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.locale = locale;
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
    
}

-(NSString*)getDateAndTime:(NSString *)aDatefromServer{
    
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    NSLocale *locale;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_KEY_LANGUAGE_CODE] isEqualToString:kLangaugeCodeOtherThanEnglish])
    {
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar"];
    }
    else
    {
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"us"];
    }
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.locale = locale;
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

@end
