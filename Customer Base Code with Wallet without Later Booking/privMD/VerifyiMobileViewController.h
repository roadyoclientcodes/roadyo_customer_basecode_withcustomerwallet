//
//  VerifyiMobileViewController.h
//  ParadiseRide
//
//  Created by Rahul Sharma on 27/03/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"

@interface VerifyiMobileViewController : UIViewController <UploadFileDelegate>

@property (weak, nonatomic) IBOutlet UITextField *verifyimobileTextField;
@property (weak, nonatomic) IBOutlet UILabel *numberMessageLabel;
@property (weak, nonatomic) IBOutlet UIButton *whyVerifyiButton;
@property (weak, nonatomic) IBOutlet UIButton *callmeButton;
@property (weak, nonatomic) IBOutlet UIButton *sendmeButton;

@property (weak, nonatomic) IBOutlet UILabel *didNotreceiveLabel;
@property (strong, nonatomic) NSString *myNumber;

@property (strong, nonatomic)  NSArray *getSignupDetails;
@property (strong,nonatomic)   NSArray *getInfoDetails;
@property (strong, nonatomic) UIImage *pickedImage;

- (IBAction)textFieldValueChanged:(UITextField *)sender;

- (IBAction)callmeButtonClicked:(id)sender;
- (IBAction)sendmemeButtonClicked:(id)sender;
@end
