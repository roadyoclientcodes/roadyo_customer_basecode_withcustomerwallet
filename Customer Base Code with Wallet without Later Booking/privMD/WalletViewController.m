//
//  WalletViewController.m
//  RoadyoDispatch
//
//  Created by 3Embed on 20/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "WalletViewController.h"
#import "WalletHistoryViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "PatientViewController.h"
#import "PatientGetLocalCurrency.h"

@interface WalletViewController ()<CustomNavigationBarDelegate, XDKAirMenuDelegate>
{
    float initialOffset_Y;
    float keyboardHeight;
}

@property (strong,nonatomic)  UITextField *activeTextField;
@end

@implementation WalletViewController
@synthesize mainScrollView;
@synthesize activeTextField;
@synthesize addMoneyTextField, promoCodeTextField;
@synthesize promoCodeCheckMarkBtn;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addCustomNavigationBar];
    self.addMoneyTextField.placeholder = [PatientGetLocalCurrency getCurrencyLocal:399];
    [Helper setButton:self.addMoneyBtn1 Text:[PatientGetLocalCurrency getCurrencyLocal:299] WithFont:Lato_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [Helper setButton:self.addMoneyBtn2 Text:[PatientGetLocalCurrency getCurrencyLocal:499] WithFont:Lato_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [Helper setButton:self.addMoneyBtn3 Text:[PatientGetLocalCurrency getCurrencyLocal:999] WithFont:Lato_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [Helper setToLabel:_walletLabel1 Text:NSLocalizedString(@"Do more with RMS wallet.",@"Do more with RMS wallet.") WithFont:Lato FSize:14 Color:UIColorFromRGB(0x23982B)];
    [Helper setToLabel:_walletLabel2 Text:NSLocalizedString(@"One tap access to all your daily needs",@"One tap access to all your daily needs") WithFont:Lato FSize:14 Color:UIColorFromRGB(0x23982B)];
    
    [Helper setToLabel:_balanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato FSize:14 Color:UIColorFromRGB(0x666666)];


    [self getWalletCurrentMoney];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    mainScrollView.backgroundColor = [UIColor clearColor];
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    mainScrollView.contentSize = CGSizeMake(screenSize.size.width, screenSize.size.height);
}

-(void)viewDidAppear:(BOOL)animated
{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


#pragma mark- Custom Methods

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"WALLET", @"WALLET")];
    [customNavigationBarView hideRightBarButton:NO];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    [self menuButtonclicked];
}

- (IBAction)historyBtnAction:(id)sender {
    [self.view endEditing:YES];
    [self performSegueWithIdentifier:@"walletHistoryVC" sender:self];

}


- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

#pragma mark - TextFields

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    [self moveViewUp:textField andKeyboardHeight:keyboardHeight];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text length] > 0)
    {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ([reachability isNetworkAvailable])
        {
            if ([textField isEqual:addMoneyTextField])
            {
                NSLog(@"Call service to add Money into wallet");
            }
            else if ([textField isEqual:promoCodeTextField])
            {
                NSLog(@"Call service to check promo code");
            }
        }
        else
        {
            [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Network", @"No Network") On:self.view];
        }
    }
    self.activeTextField = nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*--------------------------------------------*/
#pragma -marks Keyboard Appear and Hide Methods
/*--------------------------------------------*/
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification
{
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //Given size may not account for screen rotation
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self moveViewDown];
}
/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height
{
    float textfieldMaxY = CGRectGetMinY([[[textfield superview] superview] superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMinY([[textfield superview] superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMinY([textfield superview].frame);
    
    textfieldMaxY = textfieldMaxY + CGRectGetMaxY(textfield.frame);
    
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height) + initialOffset_Y;
    if (remainder >= 0)
    {
        return;
    }
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y - remainder);
                     }];
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown
{
    mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y);
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"walletHistoryVC"])
    {
        NSLog(@"Add data to next VC");
    }
}


- (IBAction)addMoneyBtn1Action:(id)sender
{
    addMoneyTextField.text = [PatientGetLocalCurrency getCurrencyLocal:299];
}

- (IBAction)addMoneyBtn2Action:(id)sender
{
    addMoneyTextField.text = [PatientGetLocalCurrency getCurrencyLocal:499];;
}

- (IBAction)addMoneyBtn3Action:(id)sender
{
    addMoneyTextField.text = [PatientGetLocalCurrency getCurrencyLocal:999];;
}

- (IBAction)promoCodeApplyBtnAction:(id)sender
{
    NSLog(@"Call service to check promo code");
}

- (IBAction)addMoneyBtnAction:(id)sender
{
    NSLog(@"Call service to add Money into wallet");
}

- (IBAction)promoCodeCheckMarkBtnAction:(id)sender
{
    if (promoCodeCheckMarkBtn.isSelected)
    {
        [UIView animateWithDuration:0.9f animations:^{
            
            [self. promoCodeView setHidden:YES];
        }];
        promoCodeCheckMarkBtn.selected = NO;
    }
    else
    {
        [UIView animateWithDuration:0.5f animations:^{
            
            [self. promoCodeView setHidden:NO];
        }];
        promoCodeCheckMarkBtn.selected = YES;
    }
}

#pragma mark - WebServices -

-(void) getWalletCurrentMoney
{
    NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
    NSString *sessionToken = [udPlotting objectForKey:KDAcheckUserSessionToken];
    NSString *sid = flStrForStr([udPlotting objectForKey:KNSUSid]);
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [udPlotting objectForKey:kPMDDeviceIdKey];
    }
    NSString *lat = @"";
    if(![udPlotting objectForKey:KNUCurrentLat])
        lat = @"";
    else
        lat = [udPlotting objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![udPlotting objectForKey:KNUCurrentLong])
        lon = @"";
    else
        lon = [udPlotting objectForKey:KNUCurrentLong];

    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_sid":flStrForStr(sid),
                             @"ent_lat":lat,
                             @"ent_lang":lon,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"GetWalletMoney"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self getWalletCurrentMoneyResponse:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Checking Current Balance...", @"Checking Current Balance...")];
}

-(void) getWalletCurrentMoneyResponse:(NSDictionary *) responseDict
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil)
    {
        return;
    }
    else if ([responseDict objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDict objectForKey: @"Error"]];
    }
    else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 ))
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    }
    else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    }
    else
    {
        self.currentBalanceLabel.text = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
    }
}

-(void) addMoneyToWallet
{
    NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
    NSString *sessionToken = [udPlotting objectForKey:KDAcheckUserSessionToken];
    NSString *sid = flStrForObj([udPlotting objectForKey:KNSUSid]);
    NSString *amount = flStrForObj(addMoneyTextField.text);
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_sid":flStrForStr(sid),
                             @"ent_amount":flStrForStr(amount),
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateDropOff"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self addMoneyToWalletResponse:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Adding Money...", @"Adding Money...")];
}

-(void) addMoneyToWalletResponse:(NSDictionary *) responseDict
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil)
    {
        return;
    }
    else if ([responseDict objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDict objectForKey: @"Error"]];
    }
    else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 ))
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    }
    else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    }
    else
    {
        
    }
}
@end
